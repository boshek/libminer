
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library setup.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("boshek/libminer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)

lib_summary()
#>                                                                                        Library
#> 1                         /Library/Frameworks/R.framework/Versions/4.4-arm64/Resources/library
#> 2 /private/var/folders/5c/fmk2pb9n0sq7cpwg4hpy65kw0000gp/T/Rtmp6WQhKQ/temp_libpathd92370633ebc
#> 3                                                                             /Users/sam/Rlibs
#>   n_packages
#> 1        171
#> 2          1
#> 3          3

## 

lib_summary(sizes = TRUE)
#>                                                                                        Library
#> 1                         /Library/Frameworks/R.framework/Versions/4.4-arm64/Resources/library
#> 2 /private/var/folders/5c/fmk2pb9n0sq7cpwg4hpy65kw0000gp/T/Rtmp6WQhKQ/temp_libpathd92370633ebc
#> 3                                                                             /Users/sam/Rlibs
#>   n_packages  lib_size
#> 1        171 417166891
#> 2          1     13944
#> 3          3   5680364
```
